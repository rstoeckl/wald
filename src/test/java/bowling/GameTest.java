package bowling;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class GameTest {

    @Test
    public void rollTwentyTimesWithZeroPins() {
        Game game = new Game();

        IntStream.range(1, 21).forEach(i -> game.roll(0));

        assertThat(game.score(), equalTo(0));
    }

    /*
     * Should pass with no exceptions
     */
    @Test
    public void roll0xFFFFTimesWithZeroPins() {
        Game game = new Game();
        for (int i = 0; i < 0xFFFF; i++)
            game.roll(0);
    }

    @Test
    public void rollThirtyTimesWithSevenPins() {
        Game game = new Game();

        for (int i = 0; i < 30; i++)
            game.roll(7);
        assertThat(game.score(), equalTo(170));
    }

    @Test
    public void rollThirtyTimesWithThreePins() {
        Game game = new Game();

        for (int i = 0; i < 30; i++)
            game.roll(3);
        assertThat(game.score(), equalTo(60));
    }

    @Test
    public void rollThirtyTimesWithTenPins() {
        Game game = new Game();

        for (int i = 0; i < 30; i++)
            game.roll(10);
        assertThat(game.score(), equalTo(300));
    }

    @Test
    public void case1() {
        assertThat(fromArray(new int[]{2, 8, 5, 2, 3, 7, 10, 6, 4, 3, 5, 4, 1, 0, 8, 9, 1, 0, 10, 6}).score(), equalTo(122));
    }

    @Test
    public void case2() {
        assertThat(fromArray(new int[]{5, 4, 10, 4, 2, 6, 1, 10, 4, 2, 0, 10, 5, 2, 3, 7, 10, 10, 10}).score(), equalTo(132));
    }

    @Test
    public void case3() {
        assertThat(fromArray(new int[]{8, 2, 1, 0, 0, 8, 10, 5, 3, 7, 3, 2, 1, 6, 4, 1, 0, 0, 7}).score(), equalTo(80));
    }

    @Test
    public void case4() {
        assertThat(fromArray(new int[]{5, 2, 10, 9, 1, 0, 0, 6, 4, 4, 2, 2, 2, 4, 4, 3, 2, 2, 4}).score(), equalTo(80));
    }

    private Game fromArray(int[] rolls) {
        Game game = new Game();
        for (int roll : rolls)
            game.roll(roll);
        return game;
    }
}
