package bowling;

public class Game {

    private int[][] score;
    private static final int FRAMES = 10, OPPORTUNITIES = 2, START_PINS = 10;
    private int currentFrame, currentRound;

    public Game() {
        //10 frames + 1 bonus frame
        score = new int[FRAMES + 1][];
        currentFrame = 0;
        currentRound = 0;
    }

    public void roll(int pins) {

        boolean regularRound = currentFrame < FRAMES;
        boolean bonusFrame = currentFrame == FRAMES;
        boolean roundBonus = OPPORTUNITIES - roundsUntilSpare(currentFrame - 1) > currentRound;
        if (!(regularRound || (bonusFrame && roundBonus)))
            return;

        pins = (pins < 0) ? 0 : pins;
        pins = (pins > START_PINS) ? START_PINS : pins;

        if (score[currentFrame] == null) {
            score[currentFrame] = new int[OPPORTUNITIES];
            currentRound = 0;
        }

        int downPins = frameScore(currentFrame, currentRound);

        pins = (currentFrame == FRAMES || START_PINS >= pins + downPins) ? pins : (START_PINS - downPins);
        score[currentFrame][currentRound] = pins;


        currentRound++;
        if ((currentRound == OPPORTUNITIES || pins == START_PINS) && currentFrame < FRAMES) {
            currentFrame++;
            currentRound = 0;
        }
    }

    public int score() {
        int sum = 0;
        for (int frameNumber = 0; frameNumber < FRAMES; frameNumber++) {
            sum += frameScore(frameNumber, OPPORTUNITIES);
            int bonusRolls = OPPORTUNITIES - roundsUntilSpare(frameNumber);
            for (int i = 1; bonusRolls > 0 && (frameNumber + i) <= FRAMES; i++) {
                sum += frameScore(frameNumber + i, bonusRolls);
                bonusRolls -= (roundsUntilSpare(frameNumber + i) + 1);
            }
        }
        return sum;
    }

    private int frameScore(int frameNumber, int rounds) {
        int sum = 0;
        int[] frame;
        if (frameNumber < score.length) {
            if ((frame = score[frameNumber]) != null) {
                for (int round = 0; round < rounds && round < OPPORTUNITIES; round++)
                    sum += frame[round];
            }
        }
        return sum;
    }

    private int roundsUntilSpare(int frameNumber) {
        int round = -1;
        int[] frame;
        if (frameNumber >= 0 && frameNumber < FRAMES && (frame = score[frameNumber]) != null) {
            int score = 0;
            while (score < START_PINS && (round + 1) < OPPORTUNITIES) {
                score += frame[round + 1];
                round++;
            }
            if (score != START_PINS)
                round++;
        }
        return round;
    }
}